# ATLASPix3 Python Analysis Software

Library of analysis software for analysis of files produced by GECCO for trimming, readout of ATLASPix3 HV-CMOS sensor.

Python software available in repository for future use.

**fit_threshold.ipynb** is a Jupyter notebook used to fit a Gaussian to the threshold distribution returned by GECCO, and obtain the parameters of this Gaussian fit.

**occupancy_map.py** is a program written to produce a hitmap for ATLASPix3 readout data.

**trimming_hitograms.py** is a program to plot 1D and 2D histograms of TDAC and threshold distributions, in order to verify that trimming has been successful.

**s_curves_select.py** is a program to plot S-curves overlaid for both optimised TDAC (post-trimming), and a selected TDAC (pre-trimming). Also fits S-curve data and returns calculated noise and threshold for optimised TDAC. Will write these to a CSV file for further analysis. Will also write noise returned by GECCO S-curve fit into CSV.

**tot_distribution.py** parses the hit data that has been written to a .dat file, and plots the ToT distribution. This can be for one file and/or multiple files to superimpose the ToT on one histogram for comprison.


