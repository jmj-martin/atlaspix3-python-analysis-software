#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 17 14:09:07 2021

@author: juliettemartin

This script parses the hit data that has been written to a .dat file, and plots an occupancy map in the form of a 2D histogram
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas
import matplotlib

def get_data(filename):
    
    #index_col set to False or won't read the .dat column 0 (corresponds to column)
    #file is tab separated
    #read hit data from CSV
    data = pandas.read_csv(filename, sep = "\t", index_col=False)
    
    #get row and column data into arrays
    row = np.array(data['Row;'])
    column = np.array(data['# Column;'])

    print(filename + ' number of events: '+ str(len(row)))
    
    return column, row

#plot the 2D histogram (occupancy) and save to file
def plot_occupancy(column, row):
    
    #each pixel is one bin
    plt.hist2d(column, row, bins = (131, 371), range = [[0, 131], [0,371]], norm=matplotlib.colors.LogNorm(vmin=1, vmax= 45))
    plt.colorbar(label = 'Counts')
    plt.xlabel('Column')
    plt.ylabel('Row')
    header = '$^{90}$Sr (trimmed to 0.5V, HV= 0V) 30 min exposure' #can change header :)
    plt.title(header)
    plt.savefig(header+'.png') #remember to save




#run program
def main():
    
    #get row, column of occupancy map
    column, row = get_data('Sr90_HV0_30m_W1M9_TDACp5.dat')
    #plot
    plot_occupancy(column, row)

    
main()
