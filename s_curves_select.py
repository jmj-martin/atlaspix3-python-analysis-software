#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 22 16:33:58 2021

@author: juliettemartin

Program to plot S-curves overlaid for both optimised TDAC (post-trimming), and a selected TDAC (pre-trimming). Default is 6.
Also fits S-curve data and returns calculated noise and threshold for optimised TDAC. Writes these to CSV, as well as noise returned by GECCO, available for further analysis.
Uses curve_fit which is not great so possible improvement could be to use another fitting algorithm
Expect this to take about 4 minutes to run
"""
import numpy as np
import matplotlib.pyplot as plt
import glob
import matplotlib
import scipy.optimize as opt
import re
from scipy.special import erf
import time
import csv

#function to collect all filenames
def collect_filenames():
    #there are 371 rows on ATLASPix3
    row_index = np.arange(0, 372, 1)
    glob_name = []
    
    for num in row_index:
        
        file = './*row' + str(row_index[num]) + '.dat'
        g = glob.glob(file)
        glob_name.append(g)

    #flatten list
    glob_name = flatten_list(glob_name)

    unwanted = './'

    #get rid of ./ in filename

    for i in range(len(glob_name)):
    
        for char in glob_name[i]:
        
            glob_name[i] = glob_name[i].replace(unwanted, '')

    return glob_name

#split strings by tab
def tidy_string(string):
    
    li = list(string.split("\t"))    
    
    return li 

#flatten the list of sublists into one long list
def flatten_list(big_list):
    
    flat_list = []
    
    for sublist in big_list:
        
        for item in sublist:
            
            flat_list.append(item)
    
    return flat_list

#collect final trimming results from GECCO (4 columns at end of file starting with ##)
def collect_data(filename):
    
    filename = filename
    
    
    test = []

    with open(filename, 'r') as f:
        
        for line in f:
            
            #get summary for selected TDAC values
            if '##' in line:    

                test.append(str(line))

    new = []

    
    #tidy everything up to have a list with items list of 4
    #list of 4 : col, row, TDAC, thresh (V)
    for j in range(len(test)):
        
        item = test[j]
        new_item = tidy_string(item)
        new.append(new_item)
        

    for k in range(len(new)):
        
        column = new[k][0]
        row =  new[k][1]
        TDAC = new[k][2]
        volt = new[k][3]
        #get rid of \n and ##
        column = column[4:]
        volt = volt[:-1]
        
        new[k][0] = float(column)
        new[k][1] = float(row)
        new[k][2] = float(TDAC)
        new[k][3] = float(volt)
        
    relevant_data = new
    
    return relevant_data


#get relevant S-curve data from lines not containing some key words
#this is one big christmas tree, but scared to touch it, as it works
#looping over a list made it throw a tantrum and idk why
#the garbage stays, for now
def read_scurves(filename):

    tdac_pix = []

    with open(filename, 'r') as f:
        
        for line in f:
            
            if '##' not in line:
                
                if 'Column' not in line:
                    
                    if 'Threshold' not in line:
                        
                        if 'oise' not in line:
                    
                            if 'Chosen' not in line:
                                
                                if 'inj' not in line:
                                
                                    if line != '\n':
                
                                        tdac_pix.append(line)
                                    

    return tdac_pix

#the list of lists of lists is inefficient but a way to partition the data into a list
#the data is in form of a long list of all S-curve data ['Scurve:pixel ()...', '0.211765\t0\n', ...]
#the task is to split into threee lists : one containing the pixel location etc, one containing signal voltage, one containing no. signals 
def split_data(tdac_pix):
    
    pix = []
    
    for i in range(len(tdac_pix)):
        #get pixel label e.g. '# SCurve: Pixel (99|371) for TDAC=3'
        if 'Pixel' in tdac_pix[i]:
            
            pix.append(tdac_pix[i]) #add label to list
    
    #create list of empty lists of length of pix (no. S-curves fitted)
    storage = [[] for _ in range(len(pix))]
    
    #counter to append data to right index of list of lists
    #idea is that it will tick up when we have finished collecting one dataset and can append data for next s-curve to next empty list
    counter = -1 #start at -1 so we can immediately tick to 0 and don't have to write a separate loop
    
    
    for j in range(len(tdac_pix)):
        
        if 'TDAC' in tdac_pix[j]:
            
            counter += 1 #tick to next index once we meet an item containing 'TDAC' indicating it is a label
            
        storage[counter].append(tdac_pix[j])
        
    
    for k in range(len(storage)):
        
        storage[k] = storage[k][1:] #first item in list of lists will be label, discard this
        
        for l in range(len(storage[k])):
            
            item = storage[k][l]
            storage[k][l] = item[:-1] #remove '\n' character

            li = tidy_string(storage[k][l]) #tab separate
            
            storage[k][l] = li
            
    for a in range(len(storage)):
        
        for b in range(len(storage[a])):
            
            for c in range(len(storage[a][b])):
                
                storage[a][b][c] = float(storage[a][b][c]) #convert to float
    
    #resulting 'storage' is list that has a list as each item of form [[voltages][signals]]
    #each of these items is full S-curve data for ONE pixel
    return storage, pix

#plot pre-trimming S-curves at a given TDAC defined in this function
def select_tdac(storage, pix):
    
    tdac = 6 #TDAC of interest, can change
    
    #all S-curves contain marker line that has pixel location and 'for TDAC= ...'
    lookup = 'for TDAC=' + str(tdac) #create lookup to identify indices of S-curve data at that particular TDAC
    
    x = []
    y = []
    
    for i in range(len(pix)):
        
        if lookup in pix[i]: #if lookup matches, S-curve data at that index is at TDAC of interest
            
            dataset = storage[i] #matching index
            
            for j in range(len(dataset)):
                
                x.append(dataset[j][0]) #this is the voltages of interest at that TDAC
                y.append(dataset[j][1])
                
    counter = 0      
    for k in range(len(y)):
        
        if y[k] > 64:
            
            counter += 1
            
    percentage = (counter/len(y))*100
    
    print('TDAC=' + str(tdac))
    
    print(len(x), '<-- number of entries')
    print('number of entries overshot = ', counter)
    print('this is ' + str(percentage) + '% of entries')
    
    #plot         
    plt.hist2d(x, y, bins = (25,55), cmap = 'viridis', norm=matplotlib.colors.LogNorm())
    plt.colorbar()
    plt.title('S-curves for TDAC=' + str(tdac) + ', HV=-60V')
    plt.xlabel('Voltage (V)')
    plt.ylabel('number of signals detected (of 64)')
    file_name = 'TDAC' + str(tdac) + '_SCurve.png' #name of file depends on selected TDAC
    plt.savefig(file_name) 
    plt.close()

#plot S-curve at optimum TDAC for each pixel (2D histogram)
#quantify overshoot, if applicable
def plot_optimum(optimum, storage):
    
    x = []
    y = []
    counter = 0
    
    for i in range(len(optimum)):
        
        index = optimum[i]
        s_curve = storage[index]
        
        for j in range(len(s_curve)):
            
            sig = s_curve[j][1]
            x.append(s_curve[j][0])
            y.append(sig)
            
            #check overshoot, can change to max. number of injections
            if sig > 64:
                
                counter += 1
    
    #plot
    plt.hist2d(x, y, cmap = 'viridis', bins = (20,50), norm=matplotlib.colors.LogNorm())
    plt.colorbar()
    plt.title('S-curves for optimised TDAC, HV=-60V')
    plt.xlabel('Voltage (V)')
    plt.ylabel('number of signals detected (of 64)')
    plt.savefig('optimised_SCurve.png')
    plt.close()
    
    #quantify overshoot, if any
    percentage = (counter/len(y))*100
    print('percentage of overshoot values:', percentage)

#error function for fitting to data
def fit_erf(x, height, sig, x0):
    
    return height*(1. + erf((x-x0) / np.sqrt(2) /sig))*0.5  

#fit sigmoid function to individual S-curves
def fit_scurve(data, identifier):
    
    x = [] #this will contain voltages
    y = [] #this will contain number of 
    
    for i in range(len(data)):
        
        x.append(data[i][0])
        y.append(data[i][1])
    
    #search for dataset matching marker
    s = identifier
    result = re.search(r'\((.*?)\)',s).group(1) 
    
    #perform sigmoid fit to S-curve data
    (a_, b_, c_), _ = opt.curve_fit(fit_erf, x, y, p0 = [70, 0.01, 0.5])
    label = 'Pixel '+ '(' + result + '), ' + 'TDAC=' + str(identifier[-2]) #label to plot an individual S_curve
    y_model = fit_erf(x, a_, b_, c_) #model based on fitted parameters, could plot if you wanted
    
    #return noise and threshold for plotting
    noise = b_
    thresh = c_
    
    return noise, thresh 

#get 'label' lines in data files containing pixel location and TDAC
def get_rawdata(filename):
    
    data_raw = []
    
    with open(filename, 'r') as f:
        
        for line in f :
            
            if '# SCurve: Pixel (' in line:
                
                data_raw.append(line)

    return data_raw

#get lines containing noise data out of data file
def get_noise(filename):
    
    noise_raw = []
    
    with open(filename, 'r') as f:
        
        for line in f :
            
            if "Noise" in line:
                
                noise_raw.append(line)

    return noise_raw

#plot histogram of noise returned by GECCO
def plot_noise_trimmed(optimum, noise):
    
    hist_data = []
    
    for i in range(len(optimum)):
        
        hist_data.append(noise[optimum[i]])
        
    print(len(hist_data))
        
    plt.hist(hist_data, bins = 55)
    plt.xlabel('Noise (V)')
    plt.ylabel('Occurrences')
    plt.title('Noise distribution post-trimming to 0.5 V')
    plt.savefig('noise_trimmed.png')
    plt.close()

  
    with open('noise_HV60.csv', 'w') as f:
        writer = csv.writer(f)
        for val in hist_data:
            writer.writerow([val])
            
#run everything
def main():
    
    start_time = time.time()    #start clock
    
    #collect list of filenames containing trimming data
    filenames = collect_filenames()
    
    big_data = [] #this is to store S-curve data
    raw_lookups = [] #store labels
    noise = [] #store noise lines
    
    
    for i in range(len(filenames)):

        tdac_pix = read_scurves(filenames[i]) #get scurve data
        big_data.append(tdac_pix) 
        raw_lookup = get_rawdata(filenames[i]) #get labels
        raw_lookups.append(raw_lookup)
        file_noise = get_noise(filenames[i])
        noise.append(file_noise)

    raw_lookups = flatten_list(raw_lookups)
    big_data = flatten_list(big_data)
    noise = flatten_list(noise)
    
    for z in range(len(noise)):
        
        item = noise[z]
        noise[z] = float(item[13:]) #get only number out of nosie, hard-coded, probably more efficient way to do this

    storage, pix = split_data(big_data)
    print('\n storage ready')
    #select_tdac(storage, pix)
    
    behemoth_list = [] #will be list of lists containing data to be flattened
    
    #collect raw summary data by line
    for file in range(len(filenames)):
        
        data_raw = collect_data(filenames[file])
        behemoth_list.append(data_raw)
        
    col = []
    row = []
    tdac = []
        
    
    final_data = flatten_list(behemoth_list) #flatten list of lists 
    
    #get row, column, TDAC
    for j in range(len(final_data)):
        
        col.append(final_data[j][0])
        row.append(final_data[j][1])
        tdac.append(final_data[j][2])
    
    #array for faster processing
    col = np.array(col)
    row = np.array(row)
    tdac = np.array(tdac)
    print('\n data compiled')
    
    keys = []
    
    #create lookup from GECCO returned optimised values
    for k in range(len(tdac)):
        
        lookup = '# SCurve: Pixel (' + str(int(col[k])) +'|' +str(int(row[k])) +')'  + ' for TDAC=' + str(int(tdac[k])) +'\n'
        keys.append(lookup)

    print('\n lookup ready')
    
    #get indices of optimal pixel + tdac values for plotting
    optimum = [pix.index(l) for l in keys]
    
    print('\n indices identified')
    print('\n length of indices', len(optimum))

    
    #plot S-curves for each pixel at optimum TDAC
    select_tdac(storage, pix)
    plot_optimum(optimum, storage)
    
    #plot noise for optimum TDAC and write to CSV
    plot_noise_trimmed(optimum, noise)

    #list to store calculated noise and threshold from erf fit
    calc_noise = []
    calc_thresh = []
    
    #calculate noise and threshold to see comparison obtained by GECCO
    for item in range(len(optimum)):
        
        index = optimum[item]

        #fit s_curve and return noise and threshold calculated by curve_fit
        #there may be some failed fits because curve_fit is garbage and also this data has some overshoots
        #possible improvement- use better fitting algorithm than curve_fit
        noise_calc, thresh_calc = fit_scurve(storage[index], pix[index])
        
        #return calculated values to list for plotting
        calc_noise.append(noise_calc)
        calc_thresh.append(thresh_calc)
    
    #plot histogram of calculated noise
    plt.hist(calc_noise, bins= 55, histtype = 'step')
    plt.title('curve_fit noise distribution post-trimming to 0.5 V, HV= -60V')
    plt.xlabel('Calculated noise (V)')
    plt.ylabel('Occurrences')
    plt.savefig('calculated_noise.png')
    plt.close()
    
    #write calculated noise to CSV
    with open('noise_HV60_calculated.csv', 'w') as g:
        writer = csv.writer(g)
        for ite in calc_noise:
            writer.writerow([ite])
    
    #plot histogram of calculated threshold
    plt.hist(calc_thresh, bins= 55, histtype = 'step')
    plt.title('curve_fit threshold distribution post-trimming to 0.5 V, HV=-60V')
    plt.xlabel('Calculated threshold (V)')
    plt.ylabel('Occurrences')
    plt.savefig('calculated_thresh.png')
    plt.close()
    
    #write calculated threshold to CSV
    with open('thresh_HV60_calculated.csv', 'w') as h:
        writer = csv.writer(h)
        for thing in calc_thresh:
            writer.writerow([thing])
    
    print('program runtime', "%s seconds" % (time.time() - start_time))

main()
