#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 17 14:24:36 2021

@author: juliettemartin

This script parses the hit data that has been written to a .dat file, and plots the ToT distribution
This can be for one file and/or multiple files to superimpose the ToT
TS1 = signal rising edge
TS2 = signal falling edge
Plots TS1, TS2, ToT
"""

import pandas
import numpy as np
import matplotlib.pyplot as plt


#parse file and get data for ToT
def get_tot(filename):
    
    #index_col omitted or else won't read final column (ToT) for some reason
    #file is tab separated
    data = pandas.read_csv(filename, sep = "\t")
    
    #get tot values in form ToT --> x
    tot = np.array(data['ext. ToT'])
    tot_raw = []
    
    #need to remove all string characters from ToT
    #so the 'ToT-->' bit, leaving only numbers
    for j in range(len(tot)):
    
        str = tot[j]
        item = [int(s) for s in str.split() if s.isdigit()]
        tot_raw.append(item)
        
    #list needs flattened in order to be ready to plot
    tot_plot = [val for sublist in tot_raw for val in sublist]

    
    return tot_plot

#function to plot signal falling edge timestamp, TS2  
def plot_TS2(filename):
    
    dat = pandas.read_csv(filename, sep = "\t", index_col=False)
    TS2 = dat['ToT;'] #this is relabeled TS2

    plt.hist(TS2, bins = np.arange(0,129,1), histtype='step')
    plt.xlabel('TS2')
    plt.ylabel('Counts')
    plt.title('$^{90}$Sr HV=-60V, trimmed, 30 min')
    plt.savefig('TS2_distrib_sr90_HV60_30min.png')
    plt.close()

#plot ToT distribution and save to file
def plot_tot(tot):
    
    plt.hist(tot, bins = np.arange(0,129,1), histtype='step')
    mean = np.mean(tot)
    n= len(tot)
    std = np.std(tot)
    error = std/np.sqrt(n)
    print(mean, error)
    plt.xlabel('ToT')
    plt.ylabel('Counts')
    plt.title('$^{90}$Sr HV=-60V, trimmed, 30 min')
    plt.savefig('ToT_distrib_sr90_HV60_30min.png')
    plt.close()
    
#function to plot signal rising edge timestamp, TS1    
def plot_TS1(filename):
    
    dat = pandas.read_csv(filename, sep = "\t", index_col=False)
    TS1 = dat['Timestamp;'] #this is relabeled TS1

    plt.hist(TS1, range = (-0.5, 1023.5), bins = 64, histtype='step')
    plt.xlabel('TS1')
    plt.ylabel('Counts')
    plt.title('$^{90}$Sr HV=-60V, trimmed, 30 min')
    plt.savefig('TS1_distrib__sr90_HV60_30min.png')
    plt.close()

#function to plot mean ToT as function of bias voltage
def plot_mean(mean, error):
    
    x = [0, 5, 10, 20, 40, 60] #bias voltages
    
    #set standard error as y errorbar
    plt.errorbar(x, mean, xerr=None, yerr= error, marker= 'o', ls='none', markersize = 2, label = '$^{90}$Sr')
    
    plt.legend()
    plt.xlabel('Bias voltage (V)')
    plt.ylabel('Mean ToT')
    plt.title('Mean ToT as function of applied bias voltage')
    plt.close()
    

#run everything    
def main():
    
    #filenames of data to be analysed
    filename0 = 'Sr90_HV0_30m_W1M9_TDACp5.dat'
    filename5 = 'Sr90_HV5_30m_W1M9_TDACp5.dat'
    filename10 = 'Sr90_HV10_30m_W1M9_TDACp5.dat'
    filename20 = 'Sr90_HV20_30m_W1M9_TDACp5.dat'
    filename40 =  'Sr90_HV40_30m_W1M9_TDACp5.dat'
    filename60 = 'Sr90_HV60_30m_W1M9_TDACp5.dat'
    
    #collect ToT data for each bias voltage
    tot0 = get_tot(filename0)
    tot5 = get_tot(filename5)
    tot10 = get_tot(filename10)
    tot20 = get_tot(filename20)
    tot40 = get_tot(filename40)
    tot60 = get_tot(filename60)
    
    #plot individual ToT, TS1, TS2 plots for HV60 (can be changed)
    plot_tot(tot60) #get ToT 
    plot_TS2(filename60) #get TS2
    plot_TS1(filename60) #get TS1
    
    #list containing  ToT data for each bias voltage
    tot_total = [tot0, tot5, tot10, tot20, tot40, tot60]
    labels = ['HV0', 'HV5', 'HV10', 'HV20', 'HV40', 'HV60']
    
    #list to store standard error of mean and mean ToT
    error = []
    mean_plot = []
    
    #get mean and standard error for each ToT
    for n in range(len(tot_total)):
        
        N = len(tot_total[n])
        std = np.std(tot_total[n])
        error_calc = std/(np.sqrt(N))
        error.append(error_calc)
        
        calc_mean = np.mean(tot_total[n])
        mean_plot.append(calc_mean)

    #plot superimposed ToT
    for i in range(len(tot_total)):
        mean = str(round(np.mean(tot_total[i]),2))
        label_mean = labels[i] + ' hits: ' + str(len(tot_total[i])) + ', mean ToT: ' + mean
        plt.hist(tot_total[i], bins = np.arange(0,128,1), histtype='step', label = label_mean)

    plt.legend()  
    plt.title('ToT')
    plt.xlabel('ToT')
    plt.ylabel('Counts')
    plt.savefig('composite_tot.png')  
    plt.close()

    #plot mean ToT as function of bias voltage
    plot_mean(mean_plot, error)


main()
    
