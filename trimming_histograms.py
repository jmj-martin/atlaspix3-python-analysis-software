#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 25 11:17:30 2021

@author: juliettemartin

Program to plot 1D and 2D histograms of TDAC and threshold distributions.
These results are obtained from files returned by GECCO post-trimming.
"""
#import relevant libraries
import numpy as np
import glob
import time
import matplotlib.pyplot as plt
import pandas as pd

#function to collect filenames of all files containing trimming results
def collect_filenames():
    
    #there are 371 rows
    row_index = np.arange(0, 372, 1)
    glob_name = [] #list to contain filenames
    
    #gatherrelevant filenames, add to list
    for num in row_index:

        file = './*row' + str(row_index[num]) + '.dat'
        g = glob.glob(file)
        glob_name.append(g)
        
    glob_name = flatten_list(glob_name)

    pesky = './'

    #get rid of ./ in filename

    for i in range(len(glob_name)):
    
        for char in glob_name[i]:
        
            glob_name[i] = glob_name[i].replace(pesky, '')
            
    #return list of filenames
    return glob_name

#flatten a list of lists into one list
def flatten_list(big_list):
    
    flat_list = []
    
    for sublist in big_list:
        
        for item in sublist:
            
            flat_list.append(item)
    
    return flat_list

#split strings by tab
def tidy_string(string):
    
    li = list(string.split("\t")) 
    
    return li 

#extract the relevant summary data
def collect_data(filename):
    
    filename = filename
    
    test = []

    with open(filename, 'r') as f:
        
        for line in f:
            
            #summary of trimming all contain '##'
            if '##' in line:    

                test.append(str(line))

    new = []
    
    #tidy everything up to have a list with items list of 4
    #list of 4 : [col, row, TDAC, thresh (V)]
    for j in range(len(test)):
        
        item = test[j]
        new_item = tidy_string(item)
        new.append(new_item)
        

    for k in range(len(new)):
        
        column = new[k][0]
        row =  new[k][1]
        TDAC = new[k][2]
        volt = new[k][3]
        
        #get rid of \n and ##
        column = column[4:]
        volt = volt[:-1]
        
        new[k][0] = float(column)
        new[k][1] = float(row)
        new[k][2] = float(TDAC)
        new[k][3] = float(volt)
        
    relevant_data = new
    
    return relevant_data

#plot 2D maps of threshold and TDAC values       
def plot_2d(full_data):

  #contruct array of zeros to represent pixel array to hold TDAC/threshold
    arr_tdac = np.zeros([372,132])
    arr_thresh = np.zeros([372,132])
    
    #replace zero with TDAC and threshold for construction of two maps
    for i in range(len(full_data)):
        
        col = int(full_data[i][0]) 
        row = int(full_data[i][1])
    
        tdac = full_data[i][2]
        thresh = full_data[i][3]
        arr_tdac[row][col] = tdac
        arr_thresh[row][col] = thresh
    
    #plot TDAC map
    plt.pcolor(arr_tdac)
    plt.colorbar().set_label('Optimised TDAC value, HV0')
    plt.title('Optimised TDAC values post-trimming')
    plt.xlabel('Column')
    plt.ylabel('Row')
    plt.savefig('optimised-tdac-map.png')
    plt.close()
    
    #plot threshold map
    plt.pcolor(arr_thresh)
    plt.colorbar().set_label('Threshold voltage (V)')
    plt.title('Optimised threshold voltage, HV0')
    plt.xlabel('Column')
    plt.ylabel('Row')
    plt.savefig('optimised-thresh-map.png')
    plt.close()

#split thresholds into corresponding TDAC values
def split_TDACs(full_data, specified_TDAC):

    thresh_tdac = []

    for l in range(len(full_data)):
        
        if full_data[l][2] == float(specified_TDAC):
                
            thresh_tdac.append(full_data[l][3])
                
    return thresh_tdac
                
#histogram of post-trimming TDAC distribution    
def hist_opt_TDAC(full_data):
    
    tdac = []
    
    for i in range(len(full_data)):
        
        tdac.append(full_data[i][2])
    
    #plot (make sure ticks aligned to centre of hist bins!)
    plt.hist(tdac, bins = 8, range= (0,8), histtype = 'step', align='left')
    plt.title('Incidences of optimised TDAC values, HV=0V')
    plt.xlabel('Optimal TDAC')
    plt.ylabel('Entries')
    plt.savefig('optimal-TDAC-distrib.png')
    plt.close()

#plot threshold distribution post-trimming as hist
def hist_threshold(full_data):
    
    thresh = []
    
    for i in range(len(full_data)):
        
        thresh.append(full_data[i][3])
        
    plt.hist(thresh, bins = 40, histtype = 'step', align = 'left')
    plt.title('Post-tuning threshold voltage distribution, HV0V')
    plt.xlabel('Threshold voltage (V)')
    plt.ylabel('Occurrences')
    plt.savefig('thresh-distrib.png')
    plt.close()
    
#write thresholds to a .csv file for further analysis    
def write_thresholds(full_data):
    
    write_threshold = [] #empty list to collect thresholds
    
    #add thresholds from GECCO results
    for i in range(len(full_data)):
        write_threshold.append(full_data[i][3])
    
    #write to csv
    df = pd.DataFrame(write_threshold)
    df.to_csv('threshold_HV0.csv', index=False)
       
#run everything       
def main():
    
    start_time = time.time()    #start clock
    filenames = collect_filenames()  #get the relevant filenames
    
    #list to collect summary data
    behemoth_list = []
    
    #get data from collected filenames
    for file in range(len(filenames)):
        
        data_raw = collect_data(filenames[file])
        behemoth_list.append(data_raw)
        
    final_data = flatten_list(behemoth_list) #flatten data to readable format
    print('\ndata collected successfully \n')
    
    hist_threshold(final_data) #plot hist of threshold post-trimming

    plot_2d(final_data) #plot 2D maps thresh/TDAC
    print('2D maps generated \n')
    

    hist_opt_TDAC(final_data) #plot optimised post-trimming TDAC distrib
    write_thresholds(final_data) #write thresholds to csv
    
    #print runtime
    print('program runtime', "%s seconds" % (time.time() - start_time))
    
main()
